﻿using UnityEngine;
using System.Collections.Generic;

public class Worker : AbstractAnt {
    public enum Action
    {
        FIND_FOOD, RETURN_WITH_FOOD
    }
    long foodCapacity
    {
        get
        {
            return owningPlayer.workerFoodCapacity;
        }
    }
    
    Stack<Hex> currentPath = new Stack<Hex>();
    HashSet<Vector2> returnPath = new HashSet<Vector2>();

    [SerializeField]
    Action currentAction = Action.FIND_FOOD;
    private long carriedFood = 0;

    public GameObject foodSourcePrefab;

    private void Start()
    {
        currentPath.Push(movement.currentHex);
        GetComponent<Health>().OnDeath.AddListener(Die);
    }

    public override void Die()
    {
        GetComponent<Health>().OnDeath.RemoveListener(Die);
        if(owningPlayer.HUD)
        {
            owningPlayer.HUD.antsCount[(int)AntType.WORKER]--;
            owningPlayer.HUD.UpdateResourcesPanel();
        }
        if (carriedFood > 0)
        {
            movement.currentHex.child = Instantiate(foodSourcePrefab);
            movement.currentHex.FixChild();
            movement.currentHex.child.GetComponent<Food>().foodAvailable = carriedFood;
        }
        Destroy(this.gameObject);
    }

    public void Update()
    {
        if(!movement.isMoving)
        {
            switch (currentAction)
            {
                case Action.FIND_FOOD:
                    Explore();
                    break;
                case Action.RETURN_WITH_FOOD:
                    ReturnWithFood();
                    break;
            }
        }
    }

    private void Explore()
    {
        Hex[] neighbours = GetNeighbouringHexes(h => !h.isEmpty);
        
        Hex target = null;

        //check if there's food on surrounding hex
        target = System.Array.Find(neighbours, h => HasFood(h));
            
        if (target != null)
        {
            //Grab some food, when you're near it
            movement.RotateTowards(target.transform.position);
            carriedFood = target.child.GetComponent<Food>().GetFood(foodCapacity);
            currentAction = Action.RETURN_WITH_FOOD;
        }
        else
        {
            //Look for it elsewhere
            Hex[] availableNeighbours = GetNeighbouringHexes(h => (h.isEmpty && !currentPath.Contains(h)));
            if (availableNeighbours.Length != 0)
            {
                //do I try to make a new path?
                if (Chance(owningPlayer.antExplorationFactor))
                {
                    target = RandomElement(availableNeighbours);
                }
                else
                {
                    target = HighestPheromone(availableNeighbours, FOOD);
                }
                movement.StartMovingTo(target);
                currentPath.Push(target);
            }
            else
            {
                //like really, really elsewhere
                //currentAction = Action.RETURN_WITH_FOOD;
                availableNeighbours = GetNeighbouringHexes(h => h.isEmpty);
                target = RandomElement(availableNeighbours);
                currentPath.Push(target);
                movement.StartMovingTo(target);
            }
        }
    }

    private void ReturnWithFood()
    {
        if (currentPath.Count > 0)
        {
            //backtrack to next square. TODO: go in it's direction if not empty
            Hex nextMove = currentPath.Peek();
            movement.StartMovingTo(nextMove);
            //add it to unique set of hexes to add pheronome to later
            returnPath.Add(nextMove.coordinates);

            currentPath.Pop();
        }
        else if (IsNearColony())
        {
            //return food to the colony
            //Debug.Log("Returned to colony with " + carriedFood + " food.");
            owningPlayer.food += carriedFood;

            //mark path with pheromone
            foreach(Vector2 el in returnPath)
            {
                ModifyPheronome(el, FOOD, carriedFood / returnPath.Count);
            }

            returnPath.Clear(); //shouldn't be needed, as ants are destroyed after returning with/-out food

            carriedFood = 0;
            currentAction = Action.FIND_FOOD;
            owningPlayer.colony.ReturnAntToColony(AntType.WORKER, this.gameObject);
        }
    }

    private bool IsNearColony()
    {
        if (currentPath.Count == 0) return true;
        System.Predicate<Hex> hasFriendlyColony = h =>
        {
            if (h.child != null)
            {
                Colony colony = h.child.GetComponent<Colony>();
                if (colony != null)
                {
                    if (colony.playerData == owningPlayer) return true;
                }
            }
            return false;
        };

        if (hasFriendlyColony(movement.currentHex)) return true;

        return System.Array.Exists(movement.currentHex.GetNeighbours(), h => hasFriendlyColony(h));
    }

    private void ReturnFromEnemy()
    {
        if (!IsNearColony())
        {
            //backtrack to next square. 
            Hex nextMove = currentPath.Peek();
            if (nextMove.isEmpty)
            {
                movement.StartMovingTo(nextMove);
                //mark it with pheromone
                ModifyPheronome(nextMove.coordinates, ENEMIES, 40.0f);
                currentPath.Pop();
            }
        }
        else
        {
            currentAction = Action.FIND_FOOD;
            owningPlayer.colony.ReturnAntToColony(AntType.WORKER, this.gameObject);
        }
    }
}
