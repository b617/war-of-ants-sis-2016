﻿using UnityEngine;
using System.Collections.Generic;

public class Hex : MonoBehaviour {

    public GameObject child = null;
    public TextMesh text;
    public Vector2 coordinates;

    public List<AbstractAnt> antsOnHex = new List<AbstractAnt>();

	public Hex[] GetNeighbours()
    {
        var grid = FindObjectOfType<HexagonalGrid>();
        List<Hex> answers = new List<Hex>();

        int xmod2 = (int)coordinates.x % 2;
        Vector2[] candidates = {
            coordinates+new Vector2(0,-1),
            coordinates+new Vector2(1,xmod2-1),
            coordinates+new Vector2(1,xmod2),
            coordinates+new Vector2(0,1),
            coordinates+new Vector2(-1,xmod2),
            coordinates+new Vector2(-1,xmod2-1)
        }; 

        foreach(Vector2 v in candidates)
        {
            Hex hex = grid.GetHex((int)v.x, (int)v.y);
            if(hex!=null)
            {
                answers.Add(hex);
            }
        }


        return answers.ToArray();
    }

    public bool isEmpty
    {
        get
        {
            return (child == null);
        }
    }

    public void FixChild()
    {
        if (child == null) return;
        child.transform.SetParent(this.transform, false);
        child.transform.localPosition = new Vector3();
    }
}




#if UNITY_EDITOR
[UnityEditor.CanEditMultipleObjects]
[UnityEditor.CustomEditor(typeof(Hex))]
public class HexCustomEditor : UnityEditor.Editor
{
    GameObject obstaclePrefab;
    bool randomizeRotation = true;
    bool randomizeColor = true;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if(GUILayout.Button("Fix child"))
        {
            (target as Hex).FixChild();
        }
        if(GUILayout.Button("List neighbours"))
        {
            Hex[] neighbours = (target as Hex).GetNeighbours();
            foreach(Hex h in neighbours)
            {
                Debug.Log(h.transform.name + ", chlid: " + h.child);
            }
        }
        GUILayout.Label("Obstacle creation:", UnityEditor.EditorStyles.boldLabel);
        obstaclePrefab = (GameObject)UnityEditor.EditorGUILayout.ObjectField(obstaclePrefab, typeof(GameObject), true);
        randomizeRotation = UnityEditor.EditorGUILayout.Toggle("Randomize rotation?", randomizeRotation);
        randomizeColor = UnityEditor.EditorGUILayout.Toggle("Randomize color?", randomizeColor);
        if(GUILayout.Button("Create child object"))
        {
            foreach(Object objHex in targets)
            {
                Hex hex = (Hex)objHex;
                GameObject newObject = Instantiate(obstaclePrefab);
                if (newObject != null)
                {
                    hex.child = newObject;
                    newObject.transform.SetParent(hex.transform, false);
                    //randomize rotation
                    if(randomizeRotation)
                    {
                        Vector3 rotation = newObject.transform.eulerAngles;
                        rotation.z = Random.Range(0.0f, 360.0f);
                        newObject.transform.eulerAngles = rotation;
                    }
                    if(randomizeColor)
                    {
                        newObject.GetComponent<SpriteRenderer>().color = new Color(Random.Range(0.6f, 0.9f), Random.Range(0.6f, 0.9f), Random.Range(0.6f, 0.9f));
                    }
                }
            }

        }
    }
}

#endif
