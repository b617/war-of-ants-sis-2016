﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;

public enum AntType
{
    WORKER,
    WARRIOR,
    LEADER
}

[RequireComponent(typeof(PlayerData))]
public class Colony : MonoBehaviour {
    
    public Hex hexPosition = null;

    public GameObject workerAntPrefab, warriorAntPrefab, leaderAntPrefab;
	
    [HideInInspector]
    public float[] unitProductionTime = { 1.0f, 2.0f, 3.0f };
    private float currentProductionTime = 0.0f;

    public Queue<AntType> productionList = new Queue<AntType>();
    private Queue<AntType> spawnList = new Queue<AntType>();
    public PlayerData playerData;

    private Health health;

    void Start()
    {
        playerData = GetComponent<PlayerData>();
        if (hexPosition == null) hexPosition = transform.parent.GetComponent<Hex>();
        transform.GetChild(0).GetComponent<SpriteRenderer>().color = playerData.playerColor;
        health = GetComponent<Health>();
        health.OnDeath.AddListener(Die);
        GetComponent<OnMouseHint>().OnHintEnabled.AddListener(UpdateHPText);
    }

    void Die()
    {
        GetComponent<Health>().OnDeath.RemoveListener(Die);
        FindObjectsOfType<AbstractAnt>().Where(a => a.owningPlayer == playerData).ToList().ForEach(h => h.Die());
        Destroy(this.gameObject);
    }


    void Update()
    {
        if(spawnList.Count>0)
        {
                AntType ant = spawnList.Dequeue();
                SpawnAnt(ant, hexPosition);
        }

        if(productionList.Count>0)
        {
            currentProductionTime += Time.deltaTime;
            AntType ant = productionList.Peek();
            if(playerData.HUD)
            {
                playerData.HUD.SetProductionProgress(currentProductionTime / unitProductionTime[(int)ant]);
            }
            if (currentProductionTime > unitProductionTime[(int)ant])
            {
                currentProductionTime = 0.0f;
                ant = productionList.Dequeue();
                spawnList.Enqueue(ant);

                if(playerData.HUD)
                {
                    playerData.HUD.SetProductionProgress(0.0f);
                    playerData.HUD.SetProductionList(productionList);
                }
            }
        }
    }

    public void AddAntToProduction(AntType ant)
    {
        productionList.Enqueue(ant);
        if(playerData.HUD)
        {
            playerData.HUD.SetProductionList(productionList);
        }
    }

    public void SpawnAnt(AntType ant, Hex hex)
    {
        GameObject GO_Ant = GetObjectFromPrefab(ant, hex.transform);
        //hex.child = GO_Ant;
        HexMovement movementComponent = GO_Ant.GetComponent<HexMovement>();
        Health hp = GO_Ant.GetComponent<Health>();
        movementComponent.currentHex = hex;
        AbstractAnt antComponent = GO_Ant.GetComponent<AbstractAnt>();
        switch (ant)
        {
            case AntType.WORKER:
                movementComponent.speed = playerData.workerMovingSpeed;
                hp.HP = playerData.workerHP;
                break;
            case AntType.WARRIOR:
                movementComponent.speed = playerData.warriorMovingSpeed;
                hp.HP = playerData.warriorHP;
                Warrior warrior = GO_Ant.GetComponent<Warrior>();
                warrior.currentAnimationGroup = (playerData.warriorsHaveSpears) ? warrior.spear : warrior.shield;
                warrior.currentAction = playerData.warriorStrategy;
                break;
            case AntType.LEADER:
                movementComponent.speed = playerData.leaderMovingSpeed;
                hp.HP = playerData.leaderHP;
                if(playerData.HUD)
                {
                    playerData.HUD.OpenLeaderControllerWindow(GO_Ant.GetComponent<Leader>());
                }
                break;
        }
        antComponent.owningPlayer = playerData;
        GO_Ant.transform.position = this.transform.position;
        GO_Ant.transform.rotation = this.transform.rotation;

        if(playerData.HUD)
        {
            playerData.HUD.antsCount[(int)ant] += 1;
            playerData.HUD.UpdateResourcesPanel();
        }

        SpriteRenderer sprite = GO_Ant.transform.GetChild(0).GetComponent<SpriteRenderer>();
        Material material = sprite.material;
        material.color = playerData.playerColor;
        sprite.material = material;
    }

    private GameObject GetObjectFromPrefab(AntType ant, Transform parent)
    {
        switch(ant)
        {
            case AntType.WORKER:
                return Instantiate(workerAntPrefab,parent,false) as GameObject;
            case AntType.WARRIOR:
                return Instantiate(warriorAntPrefab, parent, false) as GameObject;
            case AntType.LEADER:
                return Instantiate(leaderAntPrefab, parent, false) as GameObject;
            default:
                return Instantiate(workerAntPrefab, parent, false) as GameObject;
        }
    }

    public void ReturnAntToColony(AntType type, GameObject ant)
    {
        spawnList.Enqueue(type);
        AbstractAnt antComponent = ant.GetComponent<AbstractAnt>();
        antComponent.movement.currentHex.antsOnHex.Remove(antComponent);
        Destroy(ant);

        if(playerData.HUD)
        {
            playerData.HUD.antsCount[(int)type] -= 1;
            playerData.HUD.UpdateResourcesPanel();
        }
    }

    private void UpdateHPText(Text targetText)
    {
        targetText.text = health.HP.ToString() + " HP";
    }
}
