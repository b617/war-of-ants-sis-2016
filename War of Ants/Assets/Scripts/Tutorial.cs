﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;

public class Tutorial : MonoBehaviour {
    UnityEvent OnButtonOK = new UnityEvent();
    
    int tutorialState = 0;

    [Header("References")]
    public PlayerData player;
    public WinCondition winCondition;
    public Button workerCreationButton;
    public Toggle foodShowingToggle;
    public Button upgradesScreenButton;
    public Button upgradeWarriorButton;
    public Button warriorCreationButton;
    public Canvas skillTree;

    [Header("Tutorial messages")]
    public GameObject messageGameObject;
    public Text messageText;
    public GameObject tutorialArrow;

    [TextArea]
    public string[] messages;

    private void Start()
    {
        OnButtonOK.AddListener(NextTutorial);
        NextTutorial();
        
    }


    public void OnButtonConfirmationClick()
    {
        Time.timeScale = 1.0f;
        OnButtonOK.Invoke();
    }

    public void DisplayTutorialMessage()
    {
        messageText.text = messages[tutorialState];
        messageGameObject.SetActive(true);
        Time.timeScale = 0.0f;
    }

    void NextTutorialIfTrue(bool value)
    {
        if(value)
        {
            NextTutorial();
        }
    }

    IEnumerator NexTutorialWithDelay()
    {
        yield return new WaitForSeconds(1.0f);
        NextTutorial();
    }

    void NextTutorial()
    {
        switch(tutorialState)
        {
            //case 0:
            //case 1:
            //case 2:
            //case 3:
            //    break;
            case 4:
                tutorialArrow.SetActive(true);
                OnButtonOK.RemoveListener(NextTutorial);
                workerCreationButton.onClick.AddListener(NextTutorial);
                break;
            case 5:
                workerCreationButton.onClick.RemoveListener(NextTutorial);
                tutorialArrow.SetActive(false);
                OnButtonOK.AddListener(NextTutorial);
                break;
            case 6:
                OnButtonOK.RemoveListener(NextTutorial);
                foodShowingToggle.onValueChanged.AddListener(NextTutorialIfTrue);
                tutorialArrow.transform.localPosition = new Vector3(369, -52);
                tutorialArrow.SetActive(true);
                break;
            case 7:
                OnButtonOK.AddListener(NextTutorial);
                foodShowingToggle.onValueChanged.RemoveListener(NextTutorialIfTrue);
                tutorialArrow.transform.localPosition = new Vector3(238, -157);
                break;
            case 8:
                tutorialArrow.SetActive(false);
                break;
            case 9:
                OnButtonOK.RemoveListener(NextTutorial);
                tutorialArrow.transform.localPosition = new Vector3(380, 274);
                tutorialArrow.SetActive(true);
                upgradesScreenButton.onClick.AddListener(NextTutorial);
                break;
            case 10:
                upgradesScreenButton.onClick.RemoveListener(NextTutorial);
                tutorialArrow.transform.localRotation = Quaternion.Euler(0, 0, 90);
                tutorialArrow.transform.localPosition = new Vector3(-439, -50);
                upgradeWarriorButton.onClick.AddListener(NextTutorial);
                break;
            case 11:
                upgradeWarriorButton.onClick.RemoveListener(NextTutorial);
                tutorialArrow.transform.localRotation = Quaternion.identity;
                tutorialArrow.transform.localPosition = new Vector3(356.1f, -306);
                warriorCreationButton.onClick.AddListener(NextTutorial);
                break;
            case 12:
                warriorCreationButton.onClick.RemoveListener(NextTutorial);
                tutorialArrow.SetActive(false);
                OnButtonOK.AddListener(NextTutorial);
                break;

            case 16:
                OnButtonOK.RemoveListener(NextTutorial);
                if (skillTree.enabled)
                {
                    skillTree.enabled = false;
                }
                player.colony.SpawnAnt(AntType.LEADER, player.colony.hexPosition);
                StartCoroutine(NexTutorialWithDelay());
                break;
            case 17:
                OnButtonOK.AddListener(NextTutorial);
                tutorialArrow.SetActive(true);
                tutorialArrow.transform.localPosition = new Vector3(-62,-264.2f);
                break;
            case 18:
                tutorialArrow.transform.localPosition = new Vector3(100,-264.2f);
                break;
            case 19:
                tutorialArrow.transform.localPosition = new Vector3(16, -264.2f);
                break;
            case 20:
                tutorialArrow.SetActive(false);
                break;
            case 21:
                tutorialArrow.transform.localPosition = new Vector3(228, 78);
                tutorialArrow.SetActive(true);
                break;
            case 22:
                tutorialArrow.SetActive(false);
                break;
        }


        if (tutorialState >= messages.Length)
        {
            OnButtonOK.RemoveListener(NextTutorial);
            winCondition.Win();
        }
        else
        {
            DisplayTutorialMessage();
            tutorialState++;
        }
    }
}
