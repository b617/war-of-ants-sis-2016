﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

public class SkillTreeFunctions : MonoBehaviour {

    public PlayerData player;

    public void UpgradeWorker(int upgradeTier)
    {
        List<Worker> friendlyWorkers = FindObjectsOfType<Worker>().Where(a => a.owningPlayer == player).ToList();
        switch(upgradeTier)
        {
            case 0:
                player.workerFoodCapacity = 2;
                break;
            case 1:
                player.workerMovingSpeed = 10.0f;
                friendlyWorkers.ForEach(w => w.GetComponent<HexMovement>().speed = 10.0f);
                break;
            case 2:
                player.workerHP = 5;
                friendlyWorkers.ForEach(w => w.GetComponent<Health>().HP = 5);
                break;
        }
    }

    public void UpgradeWarrior(int upgradeTier)
    {
        List<Warrior> friendlyWarriors = FindObjectsOfType<Warrior>().Where(a => a.owningPlayer == player).ToList();

        switch (upgradeTier)
        {
            case 0:
                player.warriorHP = 10;
                friendlyWarriors.ForEach(w => w.GetComponent<Health>().HP = 10);
                break;
            case 1:
                player.warriorsHaveSpears = true;
                friendlyWarriors.ForEach(w => w.currentAnimationGroup = w.spear);
                break;
            case 2:
                player.warriorsSpearsCooldown /= 2;
                break;
        }
    }

    public void UpgradeLeader()
    {
        player.leaderCanKamikaze = true;
    }

    public void ToggleCanvasEnabled()
    {
        Canvas canvas = GetComponent<Canvas>();
        canvas.enabled = !canvas.enabled;
    }

    public void UnlockAnt(int antType)
    {
        player.antUnlocked[antType] = true;
        player.HUD.ValidateCreationButtons();
    }

    public void QuickerAntCreation(int upgradeTier)
    {
        if (upgradeTier == 0)
        {
            player.colony.unitProductionTime[0] = 0.5f;
        }
        else
        {
            player.colony.unitProductionTime = new float[] { 0.0f, 1.0f, 1.5f };
        }
    }

    public void IncreaseNearColonyAttack()
    {
        player.attackNearColony = 2;
    }

    public void IncreaseUnitLimis(int upgradeTier)
    {
        switch(upgradeTier)
        {
            case 0:
                player.antLimits = new int[] { 20, 6, 1 };
                break;
            case 1:
                player.antLimits = new int[] { 40, 15, 1 };
                break;
            case 2:
                player.antLimits = new int[] { 99, 99, 1 };
                break;
        }

        player.HUD.ValidateCreationButtons();
    }

    public void IncreaseColonyHP(int upgradeTier)
    {
        Health colonyHealth = player.colony.GetComponent<Health>();
        switch(upgradeTier)
        {
            case 0:
                colonyHealth.HP = 150;
                break;
            case 1:
                colonyHealth.HP = 250;
                break;
            case 2:
                colonyHealth.HP = 375;
                break;
        }
    }
}
