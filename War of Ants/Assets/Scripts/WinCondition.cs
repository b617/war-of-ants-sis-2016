﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class WinCondition : MonoBehaviour {

	public enum WinConditionType
    {
        DestroyEnemyColony,
        None
    }

    public WinConditionType winCondition = WinConditionType.DestroyEnemyColony;
    public Health enemyColony;
    public Health thisPlayerColony;

    public GameObject winLoseScreen;
    public GameObject win, lose;

    StandaloneInputModule playerInput;

    private void Start()
    {
        playerInput = GetComponent<StandaloneInputModule>();
        if (playerInput == null) 
        {
            playerInput = FindObjectOfType<StandaloneInputModule>();
        }

        if (winCondition == WinConditionType.DestroyEnemyColony) 
        {
            enemyColony.OnDeath.AddListener(Win);
        }

        thisPlayerColony.OnDeath.AddListener(Lose);
    }

    public void Win()
    {
        StartCoroutine("WinEvent");
    }

    IEnumerator WinEvent()
    {
        playerInput.enabled = false;
        yield return new WaitForSeconds(1.0f);
        //spawn "You Won!" screen here
        Debug.Log("Player won! Yupi~! \\(^^)/");
        Time.timeScale = 0.0f;
        playerInput.enabled = true;
        winLoseScreen.SetActive(true);
        win.SetActive(true);
    }

    public void Lose()
    {
        StartCoroutine("LoseEvent");
    }

    IEnumerator LoseEvent()
    {
        playerInput.enabled = false;
        yield return new WaitForSeconds(1.0f);
        //spawn "You Lost :c" screen here
        Debug.Log("Player lost. <Sadface.jpeg>");
        Time.timeScale = 0.0f;
        playerInput.enabled = true;
        winLoseScreen.SetActive(true);
        lose.SetActive(true);
    }


}
