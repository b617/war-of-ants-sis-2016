﻿using UnityEngine;
using System.Collections;

public class LeaderControllerWindow : MonoBehaviour {

    public Leader controlledLeader;

    public void Activate(Leader newLeader)
    {
        controlledLeader = newLeader;
        newLeader.GetComponent<Health>().OnDeath.AddListener(OnLeaderDeath);
    }

    private void OnLeaderDeath()
    {
        controlledLeader.GetComponent<Health>().OnDeath.RemoveListener(OnLeaderDeath);
        controlledLeader = null;
        gameObject.SetActive(false);
    }

    public void Move(int direction)
    {
        controlledLeader.Move((Leader.Direction)direction);
    }

    public void Explode()
    {
        if(controlledLeader.owningPlayer.leaderCanKamikaze)
        {
            controlledLeader.Kamikaze();
        }
    }

    public void LeaveFoodPheronome()
    {
        controlledLeader.LeavePheronome(Leader.FOOD);
    }
    public void LeaveEnemiesPheronome()
    {
        controlledLeader.LeavePheronome(Leader.ENEMIES);
    }

}
