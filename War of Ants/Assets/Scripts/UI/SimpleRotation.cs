﻿using UnityEngine;

public class SimpleRotation : MonoBehaviour {

    public Vector3 rotationSpeed;

	void Update () {
        transform.Rotate(rotationSpeed);
	}
}
