﻿using UnityEngine;
using UnityEngine.Events;

public class HexMovement : MonoBehaviour {
    public float speed = 1.0f;
    public bool isMoving { get; private set; }

    private float totalTimeNeededToMove;
    private float distance = 1.0f;
    private float timePassed;

    public Hex currentHex;
    private Hex target;
    public Hex lastPosition { get; private set; }

    [HideInInspector]
    public AbstractAnt ownerAnt;
    public UnityEvent OnMovementEnd = new UnityEvent();

    void Start()
    {
        isMoving = false;
        lastPosition = null;
        if (currentHex == null) currentHex = transform.parent.GetComponent<Hex>();
        if (currentHex == null) Debug.LogError("HexMovement Component is not a child of a hex! " + transform.name);

        currentHex.antsOnHex.Add(ownerAnt);
    }

	void Update ()
    {
	    if(isMoving)
        {
            timePassed += Time.deltaTime;

            if (timePassed >= totalTimeNeededToMove) 
            {
                StopMoving();
            }
            else
            {
                float alpha = timePassed / totalTimeNeededToMove;
                transform.position = Vector3.Lerp(currentHex.transform.position, target.transform.position, alpha);
            }
        }
        //Below: only for debug purposes. Moves randomly in any direction
        //else
        //{
        //    Hex[] emptyNeighbours = System.Array.FindAll(currentHex.GetNeighbours(), h => (h.child == null && h != lastPosition));
        //    if (emptyNeighbours.Length == 0) return;
        //    else StartMovingTo(emptyNeighbours[rand.Next(emptyNeighbours.Length)]);
        //}
	}

    public void StartMovingTo(Hex neighbouringHex)
    {
        lastPosition = currentHex;
        lastPosition.antsOnHex.Remove(ownerAnt);
        //neighbouringHex.child = this.gameObject;
        transform.SetParent(neighbouringHex.transform, true);
        isMoving = true;

        totalTimeNeededToMove = distance / speed;
        timePassed = 0.0f;

        target = neighbouringHex;
        target.antsOnHex.Add(ownerAnt);

        //TODO: Rotate towards target (rinterpto)
        RotateTowards(neighbouringHex.transform.position);
    }

    private void StopMoving()
    {
        timePassed = totalTimeNeededToMove;
        isMoving = false;
        transform.position = target.transform.position;
        //currentHex.child = null;
        currentHex = target;
        //target.child = null;
        target = null;
        OnMovementEnd.Invoke();
    }

    public void RotateTowards(Vector3 point)
    {
        Vector3 lookTarget = point - transform.position;
        if(lookTarget!=Vector3.zero)
        {
            transform.rotation = Quaternion.LookRotation(point - transform.position);
        }
    }

}
