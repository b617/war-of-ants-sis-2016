﻿using UnityEngine;

public class SimplePulsing : MonoBehaviour {
    public float speed = 1.0f;
    public Vector3 maximumOffset;
	
	// Update is called once per frame
	void Update () {
        float offset = Mathf.Sin(speed * Time.realtimeSinceStartup) + 1;
        offset /= 2;
        transform.localPosition = offset * maximumOffset;
	}
}
