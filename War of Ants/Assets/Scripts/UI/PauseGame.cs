﻿using UnityEngine;
using System.Collections;

public class PauseGame : MonoBehaviour {

	public void SetGamePaused(bool paused)
    {
        if(paused)
        {
            Time.timeScale = 0.0f;
        }
        else
        {
            Time.timeScale = 1.0f;
        }
    }

    public void ToggleGamePaused()
    {
        if (Time.timeScale == 0.0f)
        {
            Time.timeScale = 1.0f;
        }
        else
        {
            Time.timeScale = 0.0f;
        }
    }
}
