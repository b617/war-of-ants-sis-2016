﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Food : MonoBehaviour {

    public long foodAvailable;

    private void Start()
    {
        GetComponent<OnMouseHint>().OnHintEnabled.AddListener(UpdateText);
    }

    public long GetFood(long amount)
    {
        if (amount < foodAvailable)
        {
            foodAvailable -= amount;
            return amount;
        }

        amount = foodAvailable;
        foodAvailable = 0;
        StartCoroutine(DestroyThisFoodSource());
        return amount;
        //destroy object
    }

    IEnumerator DestroyThisFoodSource()
    {
        yield return new WaitForEndOfFrame();
        Destroy(this.gameObject);
    }

    public bool IsEmpty()
    {
        return foodAvailable <= 0;
    }

    private void UpdateText(Text target)
    {
        target.text = foodAvailable.ToString();
    }

}
