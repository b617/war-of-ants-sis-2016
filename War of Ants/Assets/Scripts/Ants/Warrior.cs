﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class Warrior : AbstractAnt {

    [System.Serializable]
    public struct AnimationGroup
    {
        public RuntimeAnimatorController movement, attack;
    }
    public enum Action
    {
        SCOUT,
        ATTACK_ENEMIES,
        FIGHT,
        RETURN_TO_COLONY
    }
    
    public Action currentAction = Action.SCOUT;
    [Header("Sprites")]
    [SerializeField]
    private Animator animator;
    public AnimationGroup shield, spear;
    public AnimationGroup currentAnimationGroup;


    private bool isOnAttackCooldown = false;
    private Stack<Hex> currentPath = new Stack<Hex>();
    private HashSet<Vector2> returnPath = new HashSet<Vector2>();
    private float pheromoneMarkValue = 0.0f;

    void Start()
    {
        SetAnimation(currentAnimationGroup.movement);
        GetComponent<Health>().OnDeath.AddListener(Die);
    }

    public override void Die()
    {
        GetComponent<Health>().OnDeath.RemoveListener(Die);
        if (owningPlayer.HUD)
        {
            owningPlayer.HUD.antsCount[(int)AntType.WARRIOR]--;
            owningPlayer.HUD.UpdateResourcesPanel();
        }
        Destroy(this.gameObject);
    }

    void Update()
    {
        if(!isOnAttackCooldown && !movement.isMoving)
        {
            switch (currentAction)
            {
                case Action.SCOUT:
                    Scout();
                    break;
                case Action.ATTACK_ENEMIES:
                    AdvanceToEnemies();
                    break;
                case Action.FIGHT:
                    Fight();
                    break;
                case Action.RETURN_TO_COLONY:
                    ReturnToColony();
                    break;
            }
        }

    }

    void SetAnimation(RuntimeAnimatorController anim)
    {
        animator.runtimeAnimatorController = anim;
    }

    public static void SwitchStrategy(PlayerData player,Action strategy)
    {
        FindObjectsOfType<Warrior>().Where(a => a.owningPlayer == player).ToList().ForEach(w => w.currentAction = strategy);
    }

    void Scout()
    {
        Hex[] enemies = GetNeighbouringHexes(h => HasEnemy(h));
        if(enemies.Length==0)
        {
            //is an enemy on this hex?
            enemies = HasEnemy(movement.currentHex) ? 
                new Hex[] { movement.currentHex } : new Hex[] { };
        }
        if (enemies.Length>0)
        {
            //leave approporiate mark
            if (System.Array.Exists(enemies, h => IsColony(h, false)))
            {
                pheromoneMarkValue = 100.0f;
            }
            else
            {
                pheromoneMarkValue = 10.0f;
            }
            currentAction = Action.RETURN_TO_COLONY;
            return;
        }

        //if no enemies nearby
        Explore();
    }

    void ReturnToColony()
    {
        if(currentPath.Count>0)
        {
            Hex nextMove = currentPath.Peek();
            movement.StartMovingTo(nextMove);
            returnPath.Add(nextMove.coordinates);
            currentPath.Pop();
        }
        //if is near colony
        else if(currentPath.Count==0 || IsColony(movement.currentHex,true))
        {
            foreach(Vector2 el in returnPath)
            {
                ModifyPheronome(el, ENEMIES, pheromoneMarkValue / returnPath.Count);
            }
            returnPath.Clear(); //just in case

            currentAction = Action.SCOUT;
            owningPlayer.colony.ReturnAntToColony(AntType.WARRIOR, this.gameObject);
        }
    }

    void AdvanceToEnemies()
    {
        var hexes = GetHexesInRange();
        foreach (var h in hexes)
        {
            if (IsColony(h, false) || h.antsOnHex.Find(a => a.owningPlayer != this.owningPlayer))
            {
                currentAction = Action.FIGHT;
                return;
            }
        }

        //no enemies nearby. Look for them
        Explore();

    }

    void Fight()
    {
        List<Hex> nearbyHexes = GetHexesInRange();
        Hex target;

        //has any of those hexes a enemy colony?
        target = nearbyHexes.Find(h => IsColony(h, false));
        if(target!=null)
        {
            Attack(target.child.GetComponent<Health>());
            return;
        }
        
        //no colony nearby: find enemy ant
        foreach(Hex hex in nearbyHexes)
        {
            AbstractAnt antTarget = hex.antsOnHex.Find(a => a.owningPlayer != this.owningPlayer);
            if (antTarget != null) 
            {
                Attack(antTarget.GetComponent<Health>());
                return;
            }
        }

        //no target found. 
        currentAction = owningPlayer.warriorStrategy;
    }

    void Attack(Health target)
    {
        int damage;
        if (GetNeighbouringHexes(h => IsColony(h, true)).Length > 0 || IsColony(movement.currentHex, true))
        {
            damage = owningPlayer.attackNearColony;
        }
        else
        {
            damage = 1;
        }

        target.DealDamage(damage);
        movement.RotateTowards(target.transform.position);
        StartCoroutine("AttackCooldown");
    }

    void Explore()
    {
        Hex[] availableNeighbours = GetNeighbouringHexes(h => (h.isEmpty && !currentPath.Contains(h)));
        Hex target;
        if (availableNeighbours.Length > 0)
        {
            //do i try to make a new path or expand existing one?
            if (Chance(owningPlayer.antExplorationFactor))
            {
                target = RandomElement(availableNeighbours);
            }
            else
            {
                target = HighestPheromone(availableNeighbours, ENEMIES);
            }
            movement.StartMovingTo(target);
            currentPath.Push(target);
        }
        else
        {
            //if I've already been all around here
            availableNeighbours = GetNeighbouringHexes(h => h.isEmpty);
            target = RandomElement(availableNeighbours);
            currentPath.Push(target);
            movement.StartMovingTo(target);
        }
    }

    System.Collections.IEnumerator AttackCooldown()
    {
        SetAnimation(currentAnimationGroup.attack);
        isOnAttackCooldown = true;
        yield return new WaitForSeconds(owningPlayer.warriorsAttackCooldown);
        SetAnimation(currentAnimationGroup.movement);
        isOnAttackCooldown = false;
    }

    bool IsColony(Hex hex, bool friendly)
    {
        if (hex.child == null) return false;
        Colony colony = hex.child.GetComponent<Colony>();
        if (colony == null) return false;
        if(friendly)
        {
            return (colony.playerData == owningPlayer);
        }
        else
        {
            return (colony.playerData != owningPlayer);
        }

    }

    List<Hex> GetHexesInRange()
    {
        if (owningPlayer.warriorsHaveSpears)
        {
            //todo
            return new Hex[] { movement.currentHex }.Union(movement.currentHex.GetNeighbours()).ToList();
        }
        else
        {
            return new Hex[] { movement.currentHex }.Union(movement.currentHex.GetNeighbours()).ToList();
        }
    }

}
