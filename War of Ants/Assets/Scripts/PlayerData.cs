﻿using UnityEngine;


[RequireComponent(typeof(Colony))]
public class PlayerData : MonoBehaviour {

    [Header("Initial values")]
    [SerializeField]
    private long _food = 300;
    public long food
    {
        get
        {
            return _food;
        }
        set
        {
            _food = value;
            if (HUD)
            {
                HUD.UpdateResourcesPanel();
            }
        }
    }

    public Color playerColor = Color.white;
    public HUDFunctions HUD;

    public AntType[] initialAnts;

    [Range(0.0f,1.0f)]
    public float antExplorationFactor;
    public float pheromoneDecreaseFactor = 0.05f;

    [HideInInspector]
    public Colony colony;
    [HideInInspector]
    public float[,] pheronomeFood, pheronomeEnemies;

    [Header("Ants")]
    public int[] antCosts = { 25, 50, 150 };
    public int[] antLimits = { 10, 3, 1 };
    public bool[] antUnlocked = { true, false, false };
    public int attackNearColony = 1;


    [Header("Ant Worker")]
    public long workerFoodCapacity = 1;
    public float workerMovingSpeed = 6.0f;
    public int workerHP = 2;

    [Header("Ant Warrior")]
    public bool warriorsHaveSpears = false;
    public float warriorsSpearsCooldown = 1.0f;
    public float warriorMovingSpeed = 3.0f;
    public Warrior.Action warriorStrategy = Warrior.Action.SCOUT;
    public int warriorHP = 5;
    public float warriorsAttackCooldown = 1.0f;

    [Header("Ant Leader")]
    public float leaderMovingSpeed = 2.0f;
    public int leaderHP = 1;
    public bool leaderCanKamikaze = false;


	void Start ()
    {
        colony = GetComponent<Colony>();
        Vector2 hexGridSize = FindObjectOfType<HexagonalGrid>().gridSize;
        pheronomeFood = new float[(int)hexGridSize.x, (int)hexGridSize.y];
        pheronomeEnemies= new float[(int)hexGridSize.x, (int)hexGridSize.y];

        foreach (AntType ant in initialAnts)
        {
            colony.AddAntToProduction(ant);
        }

	}

    void Update()
    {
        //pheromone evaporation
        for(int i=0;i<pheronomeFood.GetLength(0);i++)
        {
            for(int j=0;j<pheronomeFood.GetLength(1);j++)
            {
                pheronomeFood[i, j] = Mathf.Max(pheronomeFood[i, j] - (pheromoneDecreaseFactor*Time.deltaTime), 0);
                pheronomeEnemies[i, j] = Mathf.Max(pheronomeEnemies[i, j] - (pheromoneDecreaseFactor * Time.deltaTime), 0);
            }
        }
    }

}
