﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class TextEvent : UnityEvent<Text>
{
}

public class OnMouseHint : MonoBehaviour {

    public GameObject hint;
    public TextEvent OnHintEnabled = new TextEvent();
    public Text targetText;


    private void OnMouseEnter()
    {
        OnHintEnabled.Invoke(targetText);
        hint.SetActive(true);
    }
    private void OnMouseExit()
    {
        hint.SetActive(false);
    }
}
