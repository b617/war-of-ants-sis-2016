﻿using UnityEngine;
using System.Collections;

public class AI : MonoBehaviour {

    public PlayerData player;
    public float warriorsFrequency, workersFrequency;

	void Start () {
        player.warriorStrategy = Warrior.Action.ATTACK_ENEMIES;
        StartCoroutine(BuyWarrior());
        StartCoroutine(BuyWorker());
	}

    IEnumerator BuyWarrior()
    {
        while(true)
        {
            TryBuying(AntType.WARRIOR);
            yield return new WaitForSeconds(warriorsFrequency);
        }
    }

    IEnumerator BuyWorker()
    {
        while(true)
        {
            TryBuying(AntType.WORKER);
            yield return new WaitForSeconds(workersFrequency);
        }
    }

    void TryBuying(AntType ant)
    {
        if (player.food >= player.antCosts[(int)ant])
        {
            player.colony.AddAntToProduction(ant);
            player.food -= player.antCosts[(int)ant];
        }
    }
}
