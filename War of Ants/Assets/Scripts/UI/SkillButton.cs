﻿using UnityEngine;
using UnityEngine.UI;

public class SkillButton : MonoBehaviour {

    public bool unlocked = false;
    public bool bought = false;

    public Color availableToBuy;
    public Color alreadyBought;

    public int cost = 100;

    public SkillButton[] purchasesRequiredForUnlock;

    void Start()
    {
        if ((!unlocked) || (bought))
        {
            GetComponent<Button>().interactable = false;
            if(bought)
            {
                OnBuy();
            }
        }
        else
        {
            OnUnlock();
        }
    }

    PlayerData GetPlayer()
    {
        return transform.parent.parent.GetComponent<SkillTreeFunctions>().player;
    }

    public void Buy()
    {
        var player = GetPlayer();
        if(player.food>=cost)
        {
            player.food -= cost;
            bought = true;
            OnBuy();
        }

    }

    public void TryUnlocking()
    {
        foreach(SkillButton button in purchasesRequiredForUnlock)
        {
            if(button.bought==false)
            {
                return;
            }
        }

        //unlocking script
        unlocked = true;
        OnUnlock();
    }

    void OnUnlock()
    {
        Button button = GetComponent<Button>();
        button.image.color = availableToBuy;
        button.interactable = true;
    }

    void OnBuy()
    {
        Button button = GetComponent<Button>();
        button.image.color = alreadyBought;
        button.interactable = false;
    }
}
