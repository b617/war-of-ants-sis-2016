﻿using UnityEngine;


[System.Serializable]
public class HexRow
{
    public Hex[] hexes;

    public Hex this[int i]
    {
        get
        {
            return hexes[i];
        }
    }
}

public class HexagonalGrid : MonoBehaviour {

    public GameObject hexPrefab;
    public Vector2 gridSize;
    public float hexOuterRadius = 10.0f;
    [HideInInspector]
    public float hexInnerRadius
    {
        get
        {
            return hexOuterRadius * (Mathf.Sqrt(3) * 0.5f);
        }
    }

    [HideInInspector]
    public HexRow[] grid;

    //Functions executable in editor
    public Hex CreateGridTile(int posX, int posY)
    {
        Hex obj = Instantiate(hexPrefab).GetComponent<Hex>();
        obj.name = "Hex " + posX + ", " + posY;
        obj.transform.localScale = new Vector3(hexOuterRadius, hexOuterRadius, hexOuterRadius);
        obj.transform.SetParent(this.transform);
        obj.transform.position = HexPosition(posX, posY);
        obj.coordinates = new Vector2(posX, posY);
        return obj;
    }
    public void RefreshTransforms()
    {
        for(int y=0;y<grid.Length;y++)
        {
            for(int x=0;x<grid[y].hexes.Length;x++)
            {
                grid[y][x].transform.position = HexPosition(x, y);
                grid[y][x].transform.localScale = new Vector3(hexOuterRadius, hexOuterRadius, hexOuterRadius);
                grid[y][x].transform.localRotation = hexPrefab.transform.rotation;
                grid[y][x].coordinates = new Vector2(x, y);
            }
        }
    }
    private Vector3 HexPosition(int x, int y)
    {
        return new Vector3(
                    1.5f * hexOuterRadius * x,
                    0,
                    ( 2*y + (x % 2)) * hexInnerRadius);
    }
 
    //Functions executable in play

    /// <summary>
    /// Checks if exists a hex with given grid coordinates in this grid and returns it.
    /// </summary>
    /// <param name="x">column in which hex should be found</param>
    /// <param name="y">row in which hex should be found</param>
    /// <returns>Hex with this position in the array or null if there isn't one.</returns>
    public Hex GetHex(int x, int y)
    {
        if (x < 0 || y < 0 || y >= grid.Length || x >= grid[y].hexes.Length)
            return null;
        return grid[y][x];
    }
	
    /// <summary>
    /// Checks if there exists a hex with given global position in this grid and returns it.
    /// </summary>
    /// <param name="localLocation">Local position of the hex. location.y value is ignored.</param>
    /// <returns>Hex located under/on position given in location parameter or null if there isn't one.</returns>
/*TODO*/public Hex GetHexAtLocation(Vector3 localLocation)
    {
        /*
        float halfOuterRadius = hexOuterRadius / 2;
        string log = "";
        int x, z;

        if ((localLocation.x % (hexOuterRadius * 2)) - halfOuterRadius <= halfOuterRadius) 
        {
            log += "In squares. ";
            x = (int)((localLocation.x + halfOuterRadius) / (hexOuterRadius * 2));
            float offset = (x % 2 == 0) ? (-hexInnerRadius) : (0.0f);
            z = (int)((localLocation.z + offset) / (2 * hexInnerRadius));
            log += "(" + x + ", " + z + ")";
        }
        else
        {
            log += "In triangles. ";
        }

        Debug.Log(log);
        return null;
        */
        throw new System.NotImplementedException();
    }

}


#if UNITY_EDITOR

[UnityEditor.CustomEditor(typeof(HexagonalGrid))]
class HexagonalGridCustomEditor : UnityEditor.Editor
{
    HexagonalGrid hexGrid;
    UnityEditor.SerializedProperty hexes;

    public void OnEnable()
    {
        hexGrid = (HexagonalGrid)target;
        hexes = serializedObject.FindProperty("grid");
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Update grid dimensions"))
        {
            ApplyUpdatedGridSize();
        }
        if(GUILayout.Button("Refresh hex transforms"))
        {
            hexGrid.RefreshTransforms();
        }

        serializedObject.ApplyModifiedProperties();
    }

    private void ApplyUpdatedGridSize()
    {
        //ROWS

        //delete removed rows
        for(int i=hexes.arraySize-1;i>=hexGrid.gridSize.y;i--)
        {
            //delete elements
            var row = GetRow(i);
            for(int j = 0; j < row.arraySize; j++)
            {
                Hex ob = row.GetArrayElementAtIndex(j).objectReferenceValue as Hex;
                if (ob != null) DestroyImmediate(ob.gameObject);
                row.GetArrayElementAtIndex(j).objectReferenceValue = null;
            }
            //and finally the container
            hexes.DeleteArrayElementAtIndex(i);
        }

        //add added rows
        for(int i=hexes.arraySize;i<hexGrid.gridSize.y;i++)
        {
            hexes.InsertArrayElementAtIndex(i);
            GetRow(i).arraySize = 0;
        }



        //HEXES

        for(int y=0;y<hexes.arraySize;y++)
        {
            var row = GetRow(y);

            //delete removed elements
            for(int x=row.arraySize-1;x>=hexGrid.gridSize.x;x--)
            {
                Hex ob = row.GetArrayElementAtIndex(x).objectReferenceValue as Hex;
                if (ob != null) DestroyImmediate(ob.gameObject);
                row.GetArrayElementAtIndex(x).objectReferenceValue = null;

                row.DeleteArrayElementAtIndex(x);
            }

            //add new elements and recreate old ones if deleted by mistake
            for(int x=0;x<hexGrid.gridSize.x;x++)
            {
                if (x >= row.arraySize)
                {
                    row.InsertArrayElementAtIndex(x);
                    row.GetArrayElementAtIndex(x).objectReferenceValue = null;
                }

                if (row.GetArrayElementAtIndex(x).objectReferenceValue as Hex == null)
                {
                    row.GetArrayElementAtIndex(x).objectReferenceValue = hexGrid.CreateGridTile(x, y);
                }
            }
        }

    }

    private UnityEditor.SerializedProperty GetRow(int i)
    {
        return hexes.GetArrayElementAtIndex(i).FindPropertyRelative("hexes");
    }
}

#endif