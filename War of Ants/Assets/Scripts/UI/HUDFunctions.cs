﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class HUDFunctions : MonoBehaviour {
    private enum PheronomePreviewType
    {
        NONE,
        FOOD,
        ENEMIES
    }

    [HideInInspector]
    public int[] antsCount = new int[System.Enum.GetNames(typeof(AntType)).Length];

    [Header("HUD fields")]

    [SerializeField] private Text textWorkersWarriors;
    [SerializeField] private Text textFoodLeaders;
    [SerializeField] private Text textTimeScale;
    [SerializeField] private Text textAntExplorationFactor;
    [SerializeField] private Image progressBarAntCreation;
    private float progressBarAntCreationWidth;
    [SerializeField] private InputField fieldPheronomeEvaporation;
    [SerializeField] private Slider sliderPheronomeEvaporation;
    [SerializeField] private LeaderControllerWindow leaderControllerWindow;

    //[Header("Ant creation sprites")]
    public Sprite imageTextureAntNone, imageTextureAntWorker, imageTextureAntWarrior, imageTextureAntLeader;
    [SerializeField]
    private Image[] imagesAntProduction;

    [SerializeField] PlayerData player;
    public Button[] antCreationButtons;

    private HexagonalGrid hexGrid;
    private PheronomePreviewType pheronomePreviewType = PheronomePreviewType.NONE;

    private void Start()
    {
        progressBarAntCreationWidth = progressBarAntCreation.rectTransform.sizeDelta.x;
        hexGrid = FindObjectOfType<HexagonalGrid>();

        UpdateResourcesPanel();
    }

    private void Update()
    {
        UpdatePheronomePreview();
    }

    /*
     * UPPER BAR
     */


    public void UpdateResourcesPanel()
    {
        textWorkersWarriors.text = 
            "Workers: " + antsCount[(int)AntType.WORKER].ToString() + '/' + player.antLimits[(int)AntType.WORKER].ToString() + '\n'
            + '\n'
            + "Warriors: " + antsCount[(int)AntType.WARRIOR].ToString() + '/' + player.antLimits[(int)AntType.WARRIOR].ToString();
        textFoodLeaders.text = "Food: " + player.food.ToString() + '\n'
            + '\n'
            + "Leaders: " + antsCount[(int)AntType.LEADER].ToString() + '/' + player.antLimits[(int)AntType.LEADER].ToString();

        ValidateCreationButtons();
    }

    public void SetTimeScale(float value)
    {
        Time.timeScale = value;
        textTimeScale.text = value.ToString("0.00") + 'x';
    }

    public void SetAntExplorationFactor(float value)
    {
        player.antExplorationFactor = value;
        textAntExplorationFactor.text = (value * 100).ToString("0.") + '%';
    }

    /*
     * LOWER BAR
     */
    public void SetPheromoneEvaporationRate(float value)
    {
        player.pheromoneDecreaseFactor = value;
        sliderPheronomeEvaporation.value = value;
        fieldPheronomeEvaporation.text = value.ToString("0.000");
    }
    /// <summary>
    /// Ignore that. It's just there to be run by InputField for PheronomeEvaporation,
    /// and the only thing it does is run SetPheronomeEvaporationRate(float) with
    /// current InputField value
    /// </summary>
    public void SetPheronomeEvaporationRate(string value)
    {
        if (value == "") SetPheromoneEvaporationRate(0.0f);
        else SetPheromoneEvaporationRate(float.Parse(value));
    }

    public void SetProductionList(Queue<AntType> queue)
    {
        AntType[] ants = queue.ToArray();

        for(int i=0;i<imagesAntProduction.Length;i++)
        {
            Sprite targetTexture = imageTextureAntNone;
            if (i < ants.Length)
            {
                switch(ants[i])
                {
                    case AntType.WORKER:
                        targetTexture = imageTextureAntWorker;
                        break;
                    case AntType.WARRIOR:
                        targetTexture = imageTextureAntWarrior;
                        break;
                    case AntType.LEADER:
                        targetTexture = imageTextureAntLeader;
                        break;
                }
            }
            imagesAntProduction[i].sprite = targetTexture;
        }
    }

    public void SetProductionProgress(float value)
    {
        progressBarAntCreation.rectTransform.sizeDelta = 
            new Vector2(value * progressBarAntCreationWidth, progressBarAntCreation.rectTransform.sizeDelta.y);
    }

    public void OnCreateAntButton(int antType)
    {
        player.food -= player.antCosts[antType];

        AntType ant = (AntType)antType;
        player.colony.AddAntToProduction(ant);
    }
    
    public void OnPheronomeFoodToggle(bool value)
    {
        pheronomePreviewType = (value) ? (PheronomePreviewType.FOOD) : (PheronomePreviewType.NONE);
    }
    public void OnPheronomeEnemiesToggle(bool value)
    {
        pheronomePreviewType = (value) ? (PheronomePreviewType.ENEMIES) : (PheronomePreviewType.NONE);
    }

    void UpdatePheronomePreview()
    {
        for (int y = 0; y < hexGrid.grid.Length; y++)
        {
            for (int x = 0; x < hexGrid.grid[y].hexes.Length; x++)
            {
                if (hexGrid.grid[y][x].child != null) continue;
                TextMesh textMesh = hexGrid.grid[y][x].text;
                switch (pheronomePreviewType)
                {
                    case PheronomePreviewType.NONE:
                        textMesh.text = "";
                        break;
                    case PheronomePreviewType.FOOD:
                        textMesh.text = player.pheronomeFood[x, y].ToString();
                        break;
                    case PheronomePreviewType.ENEMIES:
                        textMesh.text = player.pheronomeEnemies[x, y].ToString();
                        break;
                }
            }
        }
    }

    public void SetWarriorsStrategy(int istrategy)
    {
        Warrior.Action strategy;
        if (istrategy == 0) strategy = Warrior.Action.SCOUT;
        else strategy = Warrior.Action.ATTACK_ENEMIES;

        player.warriorStrategy = strategy;
        Warrior.SwitchStrategy(player, strategy);
    }

    public void ValidateCreationButtons()
    {
        for (int i = 0; i < 3; i++)
        {
            antCreationButtons[i].interactable = (player.food >= player.antCosts[i])
                                              && (player.antUnlocked[i])
                                              && (antsCount[i] < player.antLimits[i]);
        }
    }

    public void OpenLeaderControllerWindow(Leader leader)
    {
        leaderControllerWindow.gameObject.SetActive(true);
        leaderControllerWindow.Activate(leader);
    }

    public void ReturnToMenu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
}
