﻿using UnityEngine;
using System;

[RequireComponent(typeof(HexMovement))]
public abstract class AbstractAnt : MonoBehaviour {

    [HideInInspector]
    public PlayerData owningPlayer;
    [HideInInspector]
    public HexMovement movement;
    protected static System.Random rand = new System.Random();

    public const int FOOD = 0, ENEMIES = 1;

    private void Awake()
    {
        movement = GetComponent<HexMovement>();
        movement.ownerAnt = this;
    }

    protected T RandomElement<T>(T[] tab)
    {
        return tab[rand.Next(tab.Length)];
    }

    protected float GetPheronome(Vector2 coords, int type)
    {
        float[,] tab = (type == FOOD) ? owningPlayer.pheronomeFood : owningPlayer.pheronomeEnemies;
        return tab[(int)coords.x, (int)coords.y];
    }
    protected void ModifyPheronome(Vector2 coords,int type, float value)
    {
        float[,] tab = (type == FOOD) ? owningPlayer.pheronomeFood : owningPlayer.pheronomeEnemies;
        tab[(int)coords.x, (int)coords.y] += value;
    }

    protected Hex[] GetNeighbouringHexes(Predicate<Hex> predicate)
    {
        return Array.FindAll(movement.currentHex.GetNeighbours(), predicate);
    }

    protected bool Chance(float value)
    {
        return rand.NextDouble() <= value;
    }

    protected Hex HighestPheromone(Hex[] source, int type)
    {
        System.Collections.Generic.List<Hex> results = new System.Collections.Generic.List<Hex>();
        results.Add(source[0]);
        float highestPheromone = 0;
        foreach(Hex h in source)
        {
            float pheromone = GetPheronome(h.coordinates, type);
            if (pheromone>highestPheromone)
            {
                highestPheromone = pheromone;
                results.Clear();
                results.Add(h);
            }
            else if(pheromone==highestPheromone)
            {
                results.Add(h);
            }
        }
        return results[UnityEngine.Random.Range(0,results.Count)];
    }

    protected bool HasEnemy(Hex hex)
    {
        //is there an ant colony?
        if(hex.child!=null)
        {
            Colony colony = hex.child.GetComponent<Colony>();
            if (colony != null && colony.playerData != owningPlayer) return true;
        }
        //is there an enemy ant?
        foreach(AbstractAnt ant in hex.antsOnHex)
        {
            if (ant.owningPlayer != owningPlayer) return true;
        }
        return false;
    }

    protected bool HasFood(Hex hex)
    {
        return ((hex.child != null) && (hex.child.GetComponent<Food>() != null));
    }

    public virtual void Die()
    {

    }
}
