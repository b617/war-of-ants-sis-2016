﻿using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour {

    public int HP = 0;

    public UnityEvent OnDeath = new UnityEvent();

    public void DealDamage(int damage)
    {
        //Debug.Log("Ant " + name + " takes damage. HP left: " + HP);
        HP -= damage;
        if (HP < 0)
        {
            OnDeath.Invoke();
        }
    }
    
}
