﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class Leader : AbstractAnt {
    
    public enum Direction
    {
        Up,
        Down,
        LeftUp,
        LeftDown,
        RightUp,
        RightDown
    }
        
    
    private HexagonalGrid hexGrid;
    private Direction? nextMoveBuffor = null;
    public RuntimeAnimatorController animationIdle, animationMovement;
    [SerializeField] private Animator animator;

    private void Start()
    {
        hexGrid = FindObjectOfType<HexagonalGrid>();
        MoveTo(RandomElement(GetNeighbouringHexes(h => h.isEmpty)));
        movement.OnMovementEnd.AddListener(SetAnimationtoIdle);
        SetAnimationtoIdle();
        GetComponent<Health>().OnDeath.AddListener(Die);
    }

    private void Update()
    {
        if(movement.isMoving)
        {

        }
        else
        {
            if (nextMoveBuffor != null) 
            {
                Move((Direction)nextMoveBuffor);
                nextMoveBuffor = null;
            }
        }
    }

    public void Move(Direction direction)
    {
        if(movement.isMoving)
        {
            nextMoveBuffor = direction;
            return;
        }

        Vector2 target = movement.currentHex.coordinates;
        int xmod2 = (int)target.x % 2;
        switch (direction)
        {
            case Direction.Up:
                target += new Vector2(0, 1);
                break;
            case Direction.Down:
                target += new Vector2(0, -1);
                break;
            case Direction.LeftUp:
                target += new Vector2(-1, xmod2);
                break;
            case Direction.LeftDown:
                target += new Vector2(-1, xmod2 - 1);
                break;
            case Direction.RightUp:
                target += new Vector2(1, xmod2);
                break;
            case Direction.RightDown:
                target += new Vector2(1, xmod2 - 1);
                break;
            default:
                break;
        }

        if(CanMoveTo(target))
        { 
            MoveTo(hexGrid.GetHex((int)target.x, (int)target.y));
        }
    }
    
    private void MoveTo(Hex target)
    {
        movement.StartMovingTo(target);
        SetAnimationToMovement();
    }

    private bool CanMoveTo(Vector2 coordinates)
    {
        Hex hex = hexGrid.GetHex((int)coordinates.x, (int)coordinates.y);
        if (hex == null) return false;
        return hex.isEmpty;
    }

    public void Kamikaze()
    {
        List<Hex> hexes = movement.currentHex.GetNeighbours().Union(new Hex[] { movement.currentHex }).ToList();
        List<Health> enemiesHealth = new List<Health>();
        foreach (Hex hex in hexes)
        {
            Health enemyColony = GetEnemyColony(hex);
            if (enemyColony != null) enemiesHealth.Add(enemyColony);
            hex.antsOnHex.ForEach(a =>
            {
                if (a.owningPlayer != this.owningPlayer) enemiesHealth.Add(a.GetComponent<Health>());
            });
        }

        enemiesHealth.ForEach(h => h.DealDamage(25));

        GetComponent<Health>().DealDamage(99);
    }

    private void SetAnimationtoIdle()
    {
        animator.runtimeAnimatorController = animationIdle;
    }
    private void SetAnimationToMovement()
    {
        if (animator.runtimeAnimatorController != animationMovement)
        {
            animator.runtimeAnimatorController = animationMovement;
        }
    }

    public override void Die()
    {
        GetComponent<Health>().OnDeath.RemoveListener(Die);
        movement.OnMovementEnd.RemoveListener(SetAnimationtoIdle);

        if(owningPlayer.HUD)
        {
            owningPlayer.HUD.antsCount[(int)AntType.LEADER]--;
            owningPlayer.HUD.UpdateResourcesPanel();
        }

        Destroy(this.gameObject);
    }

    public void LeavePheronome(int pheronomeType)
    {
        ModifyPheronome(movement.currentHex.coordinates, pheronomeType, 1000.0f);
    }

    Health GetEnemyColony(Hex hex)
    {
        if (hex.child == null) return null;
        Colony colony = hex.child.GetComponent<Colony>();
        if (colony == null) return null;
        return (colony.playerData != owningPlayer) ? colony.GetComponent<Health>() : null;
    }

}
